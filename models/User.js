const mongoose = require('mongoose');
const userSchema = mongoose.Schema({
    
    userName: {
        type: String,
        required : true
    },
    firstName: {
        type: String,
        required : true
    },
    lastName: {
        type: String,
        required : true
    },
    email: {
        type: String,
        required : true
    },
    password: {
        type: String,
        required : true
    },
    isAdmin: {
        type: Boolean,
        default : false
    },
    cart :[{
        id : {
            type: String,
            required : true
        },
        productName : {
            type: String,
            required : true
        },
        productDescription : {
            type: String,
            required : true
        },
        quantity : {
            type: Number,
            required : true
        },
        price : {
            type: Number,
            required : true
        },
       
    }]

});

module.exports = mongoose.model('User', userSchema);
