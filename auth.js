const jwt = require ('jsonwebtoken');
const secret = 'Nitrosphere';

module.exports.createAccessToken = () => {
    const data = {
        id : user._id,
        email : user.email,
        isAdmin : user.isAdmin
    }
    console.log(data)
    return jwt.sign (data, secret);
};

module.exports.verify = () => {
    let token = req.headers.authorization;
    if (typeof token === 'undefined'){
        return res.status(401).json({
            message: 'No token provided'
        });
    } else {
        token = token.split(' ')[1];
    }
    jwt.verify(token, secret, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                message: 'Invalid token'
            });
        } else {
            return decoded;
        }
    });

}


module.exports.verifyAdmin = (req, res, next) => {
    let token = req.headers.authorization;
    if (typeof token === 'undefined'){
        return res.status(401).json({
            message: 'No token provided'
        });
    } else {
        token = token.split(' ')[1];
    }
    jwt.verify(token, secret, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                message: 'Invalid token'
            });
        } else {
            if (decoded.isAdmin) {
                next();
            } else {
                return res.status(401).json({
                    message: 'Unauthorized'
                });
            }
        }
    });
}