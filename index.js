//! Dependencies
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const { urlencoded } = require('express');

 
//! PORT 
const PORT = 5000;
const app = express();

//! DATABASE CONNECTION 
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.hpwzu.mongodb.net/capstone-3?retryWrites=true&w=majority",
{
    useNewUrlParser : true,
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log('Successfully connected to MongoDB'));


//! ------
app.use (express.json());
app.use(urlencoded());
app.use(cors());

//! ROUTES
const 

//! PORT LISTENER
app.listen(PORT, () => {console.log(`Server is running on port ${PORT}`)});