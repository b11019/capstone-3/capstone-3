const User = require('../models/user');
const bcrypt = require('bcrypt');
const auth = require('../auth');



//! REGISTER USER
module.exports.register = (req, res) => {
    const securedPass = bcrypt.hashSync(req.body.password, 10);
    let newUser = new User({
        userName: req.body.userName,
        firstName : req.body.firstName,
        lastName : req.body.lastName,
        email : req.body.email,
        password : securedPass,
    })
    newUser.save()
    .then(user => res.send(user))
    .catch(err => res.send(err))
};

//! LOGIN USER
module.exports.login = (req, res) => {
    console.log(req.body)
    User.findOne({userName: req.body.userName})
    .then(user => {
        if(user === null){
            res.send(false)
        } else {
            const truePassword = bcrypt.compareSync(req.body.password, user.password);
        }
            if (truePassword){
                return res.send({acessToken: auth.createAccessToken(user)})
            } else {
                return res.send(false)
            }
    }
)}


//! ORDER A PRODUCT

module.exports.orderProduct =  async () => {
    console.log(req.user.id)
    console.log(req.Product.id)
    if (req.user.isAdmin){
        return res.send(false)
    }
        let userCart = await User.findById(req.user.id)
        .then(user => {
            let cart = {
                id : req.Product.id,
                productName : req.Product.productName,
                productDescription : req.Product.productDescription,
                quantity : req.Product.quantity,
                price : req.Product.price,
            }
        })
        if(userCart !== true){
            return res.send ({message: userCart})
        }
        let isOrders = await User.findById(req.body.id)
        .then(Product => {
            let orders = {
                id : req.user.id,
                quantity : req.body.quantity,
            }
            Product.orders.push(orders)
            return Product.save().then(Product => true)
            .catch(err => err.message)

        })

        if(isOrders !== true){
            return res.send ({message: isOrders})
        }
        if (userCart === true && isOrders === true){
            return res.send(true)
        }

} 
        


